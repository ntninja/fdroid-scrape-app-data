# F-Droid Google Play app metadata scraper

A simple tool to scrape the app data of any app on Google Play into a *Fastlane Supply*-like format usable by F-Droid. While this tool is primarily intended for F-Droid app maintainers it may be useful for others. If for some reason to scrape metadata in Tripple-T's Google Play uploader format please open an issue and I'll the trivial changes required to support this other format.

The entire format of the metadata scrapped is documented in moderate detail in the [F-Droid wiki](https://f-droid.org/en/docs/All_About_Descriptions_Graphics_and_Screenshots/).

## Installation

Currently only GIT installation is supported:

```shell
git clone https://gitlab.com/ntninja/fdroid-scrape-app-data.git
cd fdroid-scrape-app-data
yarn
./fdroid-scrape-app-data [--guess-langs <resource-dir>] <app-id> [<lang> [<lang> …]]
```

If you'd prefer other installation methods like `yarn` please open a MR with the necessary changes and I'll consider adding them.

## Useage

At the core of this tool is a library called [`google-play-scraper`](https://github.com/facundoolano/google-play-scraper) (shocking I know!) that will connect to Google Play's website and attempt extract metadata from the returned HTML. Aside from being unstable, this comes with another major shortcomming: As there is no language switcher on Google Play's website you'll have to provide the list of languages to scrape manually. On the bright side however, this tool can infer the list of languages to query if you have access to the source code of the app in question (decompiled will do) and if it uses Android's resource system for its translations. While this doesn't guarantee that you'll get all the translations the overlap between app translations and app website translations should be pretty high.

Based on the above there are two main ways to invoke this tool (`en-US` will always be downloaded):

  1. Download the app's metadata based on the languages in its resource directory: `./fdroid-scrape-app-data --guess-langs <resource-dir> <app-id>`
  2. Download the app's metadata for the given list of languages: `./fdroid-scrape-app-data <app-id> [<lang> [<lang> […]]]`

Note that the tool is smart about detecting duplicate translations and will automatically skip missing translations because of this.

## Screenshots and the app icon

English language screenshots and app icon will be downloaded automatically and are placed in the `phoneScreenshots` directory. No other screenshots will be downloaded.